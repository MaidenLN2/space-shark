﻿using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public GameObject player;
    public Vector3 offset;


    private void Start()
    {
        offset = new Vector3(0, 0, -10);
    }

    // Move the camera to the players postion.
    // Without rotating the camera.
    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }
}