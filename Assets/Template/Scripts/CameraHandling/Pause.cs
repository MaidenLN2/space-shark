﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public GameObject PauseObject;
    public bool isPaused = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Tab))
        {
            if (isPaused)
            {
                isPaused = false;
                PauseObject.SetActive(false);
                Time.timeScale = 1.0f;
            }
            else
            {
                isPaused = true;
                PauseObject.SetActive(true);
                Time.timeScale = 0.0f; 
            }
        }
        if (isPaused && Input.GetKeyDown(KeyCode.N))
        {
            PauseObject.SetActive(false);
            Time.timeScale = 1.0f;
        }
        if (isPaused && Input.GetKeyDown(KeyCode.Y))
        {
            Application.Quit();
        }
    }
}
