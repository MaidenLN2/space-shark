﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class zoomScript : MonoBehaviour
{

    public Camera cam;
    public float speed;

    public bool zoomOut;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (zoomOut)
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, 10f, speed);
        }
        else
        {
            cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, 6.2f, speed);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            zoomOut = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            zoomOut = false;
        }

        
    }
}
