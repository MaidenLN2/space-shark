﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AlienAttack : StateMachineBehaviour
{
    private Transform playerPos;
    private bool dashed = false;
    public float speed;
    public AudioSource audio;

  

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        audio = animator.GetComponent<AudioSource>();
        dashed = false;
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        Debug.Log(animator.GetCurrentAnimatorStateInfo(0).length);

        animator.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {        
        if (!audio.isPlaying)
        {
            audio.Stop();
            audio.Play();
        }

        if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= .6f && dashed == false)
        {
            dashed = true;
            
            animator.gameObject.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, -1) * 500);
        }
        //animator.transform.position = Vector2.MoveTowards(animator.transform.position, playerPos.position, speed * Time.deltaTime);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //   
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
