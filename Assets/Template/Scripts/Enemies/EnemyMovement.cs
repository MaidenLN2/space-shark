﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    
    public Transform player;
    public float moveSpeed = 5f;

    private Rigidbody2D rb;

    public float timeSinceDash;

    private CircleCollider2D circle;

    public GrabScript playerGrabScript;

    public LayerMask layerMask;


    public AudioSource alienIdle;
    

    Animator anim;

    // Set all the references
    void Start()
    {
        circle = this.GetComponent<CircleCollider2D>();
        anim = this.GetComponent<Animator>();
        rb = this.GetComponent<Rigidbody2D>();
        alienIdle = this.gameObject.transform.GetChild(0).GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Cast a Raycast out the front of the alien.
        RaycastHit2D raycast = Physics2D.Raycast(transform.position, transform.up * -1, 8, ~layerMask);

        // if the raycast hits the player, and its been enough time since last attack
        // Dash towards the player and reset the time since last dash.
        // This is done by initializing the attack animation then handles the attack in the state machine.
        if (raycast.collider != null && raycast.collider.tag == "Player")
        {
            if (timeSinceDash >= 3.0f)
            {
                anim.SetTrigger("Attack");
                alienIdle.Pause();
                timeSinceDash = 0.0f;
            }
            
        }
        // Increase time since last dash
        timeSinceDash += Time.deltaTime;
        if(anim.GetCurrentAnimatorStateInfo(0).IsName("Animator_AlienIDLE"))
        {
            alienIdle.UnPause();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        // If the player enters the cirlce radius around the alien, the alien will look towards the player.
        if (collision.tag == "Player")
        {
            Vector3 direction = player.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            rb.rotation = angle + 90;
        }
    }

    // The Alien hitting a player a parcel.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // If the alien hits the player, drop the players package and increase corruption.
        if (collision.collider.tag == "Player")
        {
            
            collision.gameObject.GetComponent<GrabScript>().dropPackage();
            collision.gameObject.GetComponent<Jumper>().increaseCorruption(10);
        }
        // If the alien hits the package the player is holding, drop the pacakge.
        else if (collision.collider.tag == "Parcel" && collision.gameObject.GetComponent<Parcel>().isHeld)
        {
            collision.gameObject.GetComponent<Parcel>().isHeld = false;
            playerGrabScript.dropPackage();
        }
    }


    // Debug feature to show relevant information to the developer
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawLine(transform.position, transform.position + (transform.up * -1) * 8);
    }
}
