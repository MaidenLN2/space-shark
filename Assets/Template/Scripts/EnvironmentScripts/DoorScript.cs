﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public Animator anim;


    AudioSource audioSource;

    public AudioClip doorOpening;
    public AudioClip doorClosing;

    public int requiredCorruption;

    
    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
        audioSource = this.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && collision.GetComponent<Jumper>().corruption >= requiredCorruption)
        {
            anim.SetTrigger("Open");
            audioSource.pitch = 1.3f;
            audioSource.PlayOneShot(doorOpening);
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" && collision.GetComponent<Jumper>().corruption >= requiredCorruption)
        {
            anim.SetTrigger("Close");
            audioSource.pitch = 1.2f;
            audioSource.PlayOneShot(doorClosing);
        }
        
    }
}
