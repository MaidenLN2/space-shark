﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
//using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerShipScript : MonoBehaviour
{
    public GameObject player;

    public GameObject endCutscene;

    private bool ZoomOut;
    private bool ZoomIn;

    // Start is called before the first frame update
    void Start()
    {
        ZoomOut = false;
        ZoomIn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (ZoomOut)
        {
            player.transform.GetChild(4).transform.localScale = Vector3.Lerp(player.transform.GetChild(4).transform.localScale, new Vector3(7, 7, 1), .025f);
            if (player.transform.GetChild(4).transform.localScale.x >= 6.9 && player.transform.GetChild(4).transform.localScale.y >= 6.9)
            {
                player.transform.GetChild(4).transform.localScale = new Vector3(7, 7, 1);
                ZoomOut = false;
            }
        }
        
        else if (ZoomIn)
        {
            player.transform.GetChild(4).transform.localScale = Vector3.Lerp(player.transform.GetChild(4).transform.localScale, new Vector3(4, 4, 1), .025f);
            if (player.transform.GetChild(4).transform.localScale.x <= 4.1 && player.transform.GetChild(4).transform.localScale.y <= 4.1)
            {
                player.transform.GetChild(4).transform.localScale = new Vector3(4, 4, 1);
                ZoomIn = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && endCutscene.activeSelf == true)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            //EditorSceneManager.LoadScene(EditorSceneManager.GetActiveScene().name);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            ZoomOut = true;

            if (player.GetComponent<DeliveryTracker>().missionComplete)
            {
                player.GetComponent<Jumper>().gameObject.SetActive(false);
                player.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
                //Time.timeScale = 0.0f;
                endCutscene.SetActive(true);
                Invoke("RestartGame", 10);
            }
        }
       
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            ZoomIn = true;
        }
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //EditorSceneManager.LoadScene(EditorSceneManager.GetActiveScene().name);
    }
}
