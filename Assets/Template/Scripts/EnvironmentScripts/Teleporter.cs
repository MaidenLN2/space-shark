﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{

    public bool LeftSide;
    public bool bottom;

    public float teleportRange;

    // When the player enters the teleporter zone, get teleported to the other side.
    // This is to stop the player from flying out into space.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag != "Enemy")
        {
            if (LeftSide)
            {
                collision.transform.position = new Vector3(collision.transform.position.x + teleportRange, collision.transform.position.y, collision.transform.position.z);
            }
            else if (bottom)
            {
                collision.transform.position = new Vector3(collision.transform.position.x, collision.transform.position.y + teleportRange, collision.transform.position.z);
                //collision.transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z);
            }
            else
            {
                collision.transform.position = new Vector3(collision.transform.position.x - teleportRange, collision.transform.position.y, collision.transform.position.z);
            }
        }
    }
}
