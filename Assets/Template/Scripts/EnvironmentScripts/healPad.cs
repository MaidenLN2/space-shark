﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class healPad : MonoBehaviour
{
    // The Number of Times the player can heal
    public int charges;

    private bool isHealing;

    // Heal Time
    public float healTime;
    public float healTimer;

    public DeliveryTracker packageManager;
    
    public Jumper player;

    Vector3 healPosition;


    // Sprites for the different Charges
    SpriteRenderer render;
    public Sprite Charge_0;
    public Sprite Charge_1;
    public Sprite Charge_2;
    public Sprite Charge_3;
    public Sprite Charge_4;

    public Quaternion test;

    AudioSource healSound;

    // Start is called before the first frame update
    void Start()
    {
        healSound = gameObject.GetComponent<AudioSource>();
        test = gameObject.transform.rotation;
        healTime = 3.0f;
        charges = 0;
        render = this.gameObject.GetComponent<SpriteRenderer>();
        healPosition = this.gameObject.transform.GetChild(0).transform.position;
    }

    void Update()
    {
        // If healing is set to true, begin the healing process
        if (isHealing)
        {
            if (!healSound.isPlaying)
            {
                healSound.Play();
            }
            // This is done by locking the player in the healstation.
            // And increase the heal timer
            //player.transform.position = healPosition;
            player.transform.position = Vector3.Lerp(player.transform.position, healPosition, .05f);
            player.transform.rotation = Quaternion.Lerp(player.transform.rotation, test, .05f);
            healTimer += Time.deltaTime;



            // Check if the heal timer is greater than the heal time
            // If so, disable healing, decrease corruption of player, and decrease charges of healthstation.
            if (healTimer >= healTime)
            {
                player.allowMove = true;
                isHealing = false;
                player.decreaseCorruption(25);
                decreaseCharges();
            }
        }
    }

    private void FixedUpdate()
    {
        if (isHealing)
        {
            player.transform.position = Vector3.Lerp(player.transform.position, healPosition, .05f);
            player.transform.rotation = Quaternion.Lerp(player.transform.rotation, test, .05f);
        }
    }

    // Changes the player sprite based off of corruption level.
    // Only called when charges of healthstation are increased or decreased
    void changeSprites()
    {
        if (charges == 0)
        {
            render.sprite = Charge_0;
        }
        else if (charges == 1)
        {
            render.sprite = Charge_1;
        }
        else if (charges == 2)
        {
            render.sprite = Charge_2;
        }
        else if (charges == 3)
        {
            render.sprite = Charge_3;
        }
        else if (charges >= 4)
        {
            render.sprite = Charge_4;
            charges = 4;
        }
    }

    // Increase the number of charges in the healthstation
    // and change the health station sprite.
    public void increaseCharges()
    {
        charges += 1;
        changeSprites();
    }

    // Decrease the number of charges in the healthstation
    // and change the health station sprite.
    public void decreaseCharges()
    {
        charges -= 1;
        changeSprites();
    }

    // Begin the healing process when the player interacts with the heal pad.
    public void startHeal()
    {
        if (charges > 0 && !isHealing)
        {
            player.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            player.allowMove = false;
            healTimer = 0;
            isHealing = true;
        }
        
    }
}
