﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;
using UnityEngine.Audio;

public class Breakable : MonoBehaviour
{

    public GameObject rocklet1;
    public GameObject rocklet2;
    public GameObject rocklet3;
    public GameObject rocklet4;
    public GameObject rocklet5;
    public GameObject rocklet6;

    public GameObject jumper;

    public bool isHeld;
    bool breaking = false;

    public AudioClip audioClip;


    /*
      Collision Check for breaking
      This checks if there is a rigid body of the object with the breakable tag,
      and if there is, and it is colliding with something that is not the player,
      and the velocity is greater than 5 units, then destroy the game object.
      This allows objects to break at fast speeds but not break when the player runs into them.
    */
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Create an variable for the game objects rigid body.
        SpriteRenderer renderer = gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>();

        AudioSource audio = gameObject.GetComponent<AudioSource>();
        Rigidbody2D test = gameObject.GetComponent<Rigidbody2D>();
        // If there is a rigid body
        if (!breaking)
        {
            // If the collision is not with the player
            if (collision.gameObject.name != "Jumper")
            {
                // if the velocity of the game object is greater than 5
                if (test.velocity.magnitude > 3)
                {
                    // destroy the game object
                    Instantiate(rocklet1, transform.position + new Vector3(0.2f, 0, 0), transform.rotation);
                    Instantiate(rocklet2, transform.position + new Vector3(-0.2f, 0, 0), transform.rotation);
                    Instantiate(rocklet3, transform.position + new Vector3(0.2f, 0.2f, 0), transform.rotation);
                    Instantiate(rocklet4, transform.position + new Vector3(0.2f, -0.2f, 0), transform.rotation);
                    Instantiate(rocklet5, transform.position + new Vector3(-0.2f, 0.2f, 0), transform.rotation);
                    Instantiate(rocklet6, transform.position + new Vector3(-0.2f, -0.2f, 0), transform.rotation);

                    rocklet1.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(-1, -1) * 500);
                    rocklet2.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(-1, -1) * 500);
                    rocklet3.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(-1, -1) * 500);
                    rocklet4.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(-1, -1) * 500);
                    rocklet5.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(-1, -1) * 500);
                    rocklet6.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(-1, -1) * 500);


                    if (isHeld)
                    {

                        jumper.GetComponent<GrabScript>().dropPackage();
                        toggleHeld();

                        breaking = true;
                    }
                    else
                    {
                        breaking = true;
                    }
                    
                }
            }
        }
        else
        {
            if (!audio.isPlaying)
            {
                audio.PlayOneShot(audioClip);
                test.isKinematic = true;
                renderer.enabled = false;
                Destroy(gameObject, audioClip.length);
            }
        }
    }

    public void toggleHeld()
    {
        isHeld = !isHeld;
    }
}
