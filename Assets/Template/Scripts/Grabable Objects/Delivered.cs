﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delivered : MonoBehaviour
{
    [SerializeField] GrabScript delivery = null;

    public SpriteRenderer spriteRenderer;
    public Sprite deliveredSprite;

    public DeliveryTracker deliveryManager;
    public healPad healPad;

    [Tooltip("The Delivery ID. Must equal the parcel ID for it to be delivered")]
    public int deliveryTarget;

    public bool delivered;
    private AudioSource placeAudio;


    private void Start()
    {
        placeAudio = this.gameObject.GetComponent<AudioSource>();
    }

    /*
      Delivery Collison Check
      This function stores the rigid body of the parcel class as the parcel type.
      It then checks to make sure that the parcel type has a rigid body.
      If it does, it checks that the parcel number is equal to that of the delivery target
      If the parcel ID and delivery ID are the same, 
      It ungrabs the object, destroys the parcel object, increases delivered parcels
      and Increases charges in the healstation.
    */
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Parcel parcelType = collider.GetComponent<Parcel>();

        if (parcelType != null)
        {
            if (parcelType.getParcel() == deliveryTarget && !delivered)
            {
                delivery.Grabbed = false;
                Destroy(parcelType.gameObject);
                spriteRenderer.sprite = deliveredSprite;
                delivered = true;
                deliveryManager.deliverParcel(this.deliveryTarget);
                healPad.increaseCharges();
                placeAudio.Play();
            }
        }
    }
}
