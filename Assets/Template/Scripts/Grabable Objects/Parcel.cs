﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parcel : MonoBehaviour
{
    public int ParcelNumber;
    public bool isHeld;
    
    /* 
      Get Parcel Number
      This function returns the parcel number of the object.
      This is used with the delivered script to ensure that the correct parcel is delivered.
    */
    public int getParcel()
    {
        return ParcelNumber;
    }
}
