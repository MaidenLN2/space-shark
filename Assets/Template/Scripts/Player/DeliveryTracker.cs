﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeliveryTracker : MonoBehaviour
{
    public int deliveredParcels;

    public int totalParcels;

    public int redParcels;
    public int blueParcels;
    public int greenParcels;

    public int deliveredRed;
    public int deliveredBlue;
    public int deliveredGreen;

    public TextMesh taskText;
    private string displayText;

    public bool missionComplete = false;

    // Start is called before the first frame update
    void Start()
    {
        // This finds how many total packages there are to deliver.
        GameObject[] allTargets = GameObject.FindGameObjectsWithTag("DeliveryTarget");
        totalParcels = allTargets.Length;

        for (int i = 0; i < allTargets.Length; i++)
        {
            if (allTargets[i].GetComponent<Delivered>().deliveryTarget == 1)
            {
                blueParcels += 1;
            }
            else if (allTargets[i].GetComponent<Delivered>().deliveryTarget == 2)
            {
                redParcels += 1;
            }
            else if (allTargets[i].GetComponent<Delivered>().deliveryTarget == 3)
            {
                greenParcels += 1;
            }
        }
        UpdateText();
        Debug.Log(totalParcels);
        Debug.Log(blueParcels);
        Debug.Log(redParcels);
        Debug.Log(greenParcels);
    }

    // This function gets called when a package is delivered.
    // And checks if all packages are delivered
    public void deliverParcel(int type)
    {
        deliveredParcels += 1;
        if (type == 1)
        {
            deliveredBlue += 1;
        }
        else if (type == 2)
        {
            deliveredRed += 1;
        }
        else if (type == 3)
        {
            deliveredGreen += 1;
        }
        UpdateText();
        
        if (deliveredParcels == totalParcels)
        {
            Debug.Log("All Parcels Delivered");
        }
    }

    void UpdateText()
    {
        if (deliveredParcels < totalParcels)
        {
            displayText = "Blue Parcels = " + deliveredBlue + "/" + blueParcels + "\nRed Parcels = " + deliveredRed + "/" + redParcels + "\nGreen Parcels = " + deliveredGreen + "/" + greenParcels;
            taskText.text = displayText;
        }
        else
        {
            missionComplete = true;
            displayText = "Return to Ship";
            taskText.text = displayText;
        }
    }
}
