﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{

    // Variables Used Throughout field of view effect.
    [SerializeField] private LayerMask layerMask;
    
    private Mesh mesh;
    private Vector3 origin = Vector3.zero;
    private float StartingAngle = -135;
    public float fov = 90f;
    public float viewDistance = 100f;
    /*
      Store the Mesh upon start
    */
    private void Start()
    {
        // Upon Start set the mesh to the componenets mesh
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
    }

    /*
      This update handles the field of view being created using raycasts
    */
    private void LateUpdate()
    {
        mesh.RecalculateBounds();
        // FIELD OF VIEW VARIABLES
        // The Number of Rays in the raycast.
        int rayCount = 250;
        // The Angle that the rays start from
        float angle = StartingAngle;
        // How much each raycast should increase by
        float angleIncrease = fov / rayCount;
        // The length of the view distance.
        

        // MESH VARIABLES
        Vector3[] vertices = new Vector3[rayCount + 1 + 1];
        Vector2[] uv = new Vector2[vertices.Length];
        int[] triangles = new int[rayCount * 3];

        vertices[0] = origin;
        int vertexIndex = 1;
        int triangleIndex = 0;

        // For loop to create a Field of View using ray cast.
        for (int i = 0; i <= rayCount; i++)
        {
            Vector3 vertex;
            // Cast A RayCast in the direction of the intended FOV.
            // If it hits something, set the vertex there,
            // Otherwise Set the vertex at the view distance.
            RaycastHit2D raycastHit2D = Physics2D.Raycast(origin, GetVectorFromAngle(angle), viewDistance, layerMask);
            // NO HIT SO SET VERTEX AT VIEW DISTANCE
            if (raycastHit2D.collider == null)
            {
                vertex = origin + GetVectorFromAngle(angle) * viewDistance;
            }
            // RAYCAST HIT SO SET VERTEX TO HIT POINT
            else
            {
                
                vertex = raycastHit2D.point;
            }
            // Set the vertices of the current index to the the vertex created.
            vertices[vertexIndex] = vertex;

            // try to create a triangle fter the first vertex is created
            // Otherwise it will fuck up.
            // (Because we cant set it when "vertexIndex - 1 = -1"
            if (i > 0)
            {
                triangles[triangleIndex + 0] = 0;
                triangles[triangleIndex + 1] = vertexIndex - 1;
                triangles[triangleIndex + 2] = vertexIndex;
                triangleIndex += 3;
            }
            // Increase VertexIndex
            vertexIndex++;
            // Increase Angle in a clockwise fashion.
            angle -= angleIncrease;
        }

        // Sets the created Meshs vertices, UV and Triangles to the points created in the FOR loop.
        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = triangles;
    }

    // Sets the origin of the field of view to the passed in origin.
    // This is used to keep the origin within the center of the player.
    public void setOrigin(Vector3 origin)
    {
        this.origin = origin;
    }

    // Sets the Direction to the direction of the player.
    public void SetAimDirection(Vector3 aimDirection)
    {
        StartingAngle = GetAngleFromVectorFloat(aimDirection) - fov / 2;
    }



    // Get The Vector from an Angle
    // This function takes a given angle, and returns a vector towards it.
    public static Vector3 GetVectorFromAngle(float angle)
    {
        // Angle = 0 -> 360
        float angleRad = angle * (Mathf.PI / 180f);
        return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
    }

    // Get Angle From Vector
    // Takes a vector and returns an angle to turn towards it.
    public static float GetAngleFromVectorFloat(Vector3 dir)
    {
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0)
        {
            n += 360;
        }
        return n;
    }
}
