﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.VersionControl;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class GrabScript : MonoBehaviour
{
    [SerializeField] private LayerMask layerMask;

    [Header("Player Grabbing / Throwing")]

    [Tooltip("Whether an object has been grabbed or not")]
    public bool Grabbed;

    [Tooltip("The speed that we throw the object")]
    public float throwForce;
    [Tooltip("The location where the player holds the object.")]
    public Transform holdPoint;
    [Tooltip("The length the player can pick up from")]
    public float distance = 2f;

    // Rotation of the object picked up
    private Vector3 Rotation = Vector3.zero;
    // The angle that the object is
    private float StartingAngle;
    // Raycast of the pickup range
    RaycastHit2D hit;
    Animator hands;


    // Grabbing Audio
    AudioSource handAudio;
    public AudioClip handGrab;
    public AudioClip handThrow;


    public void Start()
    {
        // Hand Audio
        handAudio = this.gameObject.transform.GetChild(1).GetComponent<AudioSource>();
        // Hand Animation
        hands = this.gameObject.transform.GetChild(1).GetComponent<Animator>();
    }

    /*
      Grab Update Script.
      Checks if the player presses space to try to pick up an object.
      If nothing is grabbed, it casts a raycast in order to try to pick up an object.
      If the raycast hits something with a grababble tag, it picks the object up.
      If something is grabbed, it then proceeds to throw the object away from the player.

      If something has been grabbed, it keeps the rotation matching the players rotation.
    */
    void Update()
    {
        
        // Sets the Angle to the Angle of the player.
        float angle = StartingAngle;
        // Run when Space gets pressed
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // If nothing is grabbed
            if (!Grabbed)
            {

                // Raycast in the direction of the player to try to hit somethinng
                hit = Physics2D.Raycast(transform.position, GetVectorFromAngle(angle), distance, layerMask); ;

                // If it hits something that is able to be grabbed set grabbed to true
                if (hit.collider != null && (hit.collider.tag == "Grababble" || hit.collider.tag == "Parcel"))
                {
                    handAudio.PlayOneShot(handGrab, 0.5f);
                    Grabbed = true;
                    // If the object is a breakable object, set breakable held to true
                    if (hit.collider.gameObject.GetComponent<Breakable>() != null)
                    {
                        hit.collider.gameObject.GetComponent<Breakable>().toggleHeld();
                    }
                    // If the object is a parcel, set parcel held to true
                    else if (hit.collider.gameObject.GetComponent<Parcel>() != null)
                    {
                        hit.collider.gameObject.GetComponent<Parcel>().isHeld = true;
                    }
                }

                // if it hits the healstation, cause the player to start healing.
                else if (hit.collider != null && hit.collider.tag == "HealStation")
                {
                    hit.collider.gameObject.GetComponent<healPad>().startHeal();
                }
            }

            // If Grabbed is True
            else
            {
                handAudio.PlayOneShot(handThrow, 0.3f);
                // Set Grabbed to false
                hands.SetBool("isHolding", false);
                Grabbed = false;
                // If the object picked up has a rigid body
                if (hit.collider.gameObject.GetComponent<Rigidbody2D>() != null)
                {
                    // Throw the object in the direction of the player
                    hit.collider.gameObject.GetComponent<Rigidbody2D>().velocity = GetVectorFromAngle(angle) * throwForce;
                    hit.collider.gameObject.GetComponent<Rigidbody2D>().freezeRotation = false;
                    // Sets the breakable held to false
                    if (hit.collider.gameObject.GetComponent<Breakable>() != null)
                    {
                        hit.collider.gameObject.GetComponent<Breakable>().toggleHeld();
                    }
                    // Sets the parcel held to false
                    else if (hit.collider.gameObject.GetComponent<Parcel>() != null)
                    {
                        hit.collider.gameObject.GetComponent<Parcel>().isHeld = false;
                    }


                }
            }


        }

        // Once something has been grabbed, set it so player has picked it up.
        if (Grabbed)
        {
            if (hit.collider.gameObject.GetComponent<Breakable>() == null || hit.collider.gameObject.GetComponent<Breakable>().isHeld)
            {
                hands.SetBool("isHolding", true);
                hit.collider.gameObject.transform.rotation = Quaternion.Euler(Rotation);
                hit.collider.gameObject.transform.position = holdPoint.position;
                hit.collider.gameObject.GetComponent<Rigidbody2D>().freezeRotation = true;
            }
        }
        else
        {
            hands.SetBool("isHolding", false);
        }
    }

    public void dropPackage()
    {
        Grabbed = false;
        hands.SetBool("isHolding", false);
    }
    /*
      This Function sets the aim direction to the starting angle
      This is called in the jumper script in order to keep the direction of the picked up box in the direction of the player.
    */
    public void SetAimDirection(Vector3 aimDirection)
    {
        StartingAngle = GetAngleFromVectorFloat(aimDirection);
    }

    /*
      Get Vector From Angle
      A math function required for the raycasting in right directions
    */
    public static Vector3 GetVectorFromAngle(float angle)
    {
        // Angle = 0 -> 360
        float angleRad = angle * (Mathf.PI / 180f);
        return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
    }

    /*
      Get Angle from Vector
      Math function to turn an angle into a vector. Used to set aim direction of the object.
    */
    public static float GetAngleFromVectorFloat(Vector3 dir)
    {
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0)
        {
            n += 360;
        }
        return n;
    }

    /*
      Sets the rotation variable to the rotation passed in.
      This is used to set the rotation of the grabbed object to the rotation of the player.
    */
    public void SetRotation(Vector3 rotation)
    {
        Rotation = rotation;
    }

    // DEBUG FUNCTIONS
    // Gizmo
    // This Gizmo shows the reach that the player can pick up from.
    private void OnDrawGizmos()
    {
        float angle = StartingAngle;
        Gizmos.color = Color.green;

        Gizmos.DrawLine(transform.position, transform.position + GetVectorFromAngle(angle) * distance);
    }
}
