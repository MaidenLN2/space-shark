﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq.Expressions;
using System.Net.NetworkInformation;
//using UnityEditor.ShaderGraph.Internal;
using UnityEngine;
//using UnityEngine.XR.WSA.Sharing;
using UnityEngine.SceneManagement;
//using UnityEditor.SceneManagement;

public class Jumper : MonoBehaviour
{
    [SerializeField] FieldOfView viewField = null;

    [SerializeField] GrabScript grabObj = null;

    [Header("Jumping")]

    [Tooltip("The speed that we travel when we jump")]
    public float jumpStrength = 8.0f;
    [Tooltip("Whether the player is charging a jump or not")]
    private bool isCharging = false;
    [Tooltip("The Length of the Charge Timer")]
    public float chargeLength = 2.0f;
    [Tooltip("The current charge time for the players jump")]
    public float chargingTimer = 0.0f;

    public bool allowMove;

    // Variables to control the players charge delay.
    private float chargeDelay = 0.3f;
    private float chargeDelayTimer = 1.0f;
    private bool hasDashed = false;

    // Animator and AudioSources for all parts of the player.
    Animator anim;
    Animator thrusters;
    AudioSource audioBody;
    AudioSource thrusterAudio;
    AudioSource chargeAudio;
    AudioSource damageAudio;
    AudioSource bonk;

    // Audio Clips for aspects of the player
    public AudioClip dash;
    public AudioClip a_Charging;
    public AudioClip a_Charged;

    // The Dash Particle
    public GameObject dashParticle;
    private Vector3 particlePosition;

    // Damage Variables
    public float corruption;
    private float gracePeriod = 2.0f;
    public float graceTimer = 0.0f;
    public GameObject damageVisuals;
    bool isCreated = false;
    public GameObject highCorruptionVisuals;
    GameObject highCorruptionEffect;

    // Sprites and Sprite Renderer for damage states.
    SpriteRenderer damageRenderer;
    public Sprite ADamage;
    public Sprite BDamage;
    public Sprite CDamage;
    public Sprite DDamage;
    CameraShake camShake;

    // Initilize components.
    private void Start()
    {
        // hides cursor upon first frame;
        Cursor.visible = false;

        allowMove = true;

        // Body
        anim = this.gameObject.transform.GetChild(2).GetComponent<Animator>();
        chargeAudio = this.gameObject.transform.GetChild(2).GetComponent<AudioSource>();

        // Thrusters
        thrusters = this.gameObject.transform.GetChild(0).GetComponent<Animator>();
        thrusterAudio = this.gameObject.transform.GetChild(0).GetComponent<AudioSource>();

        // Damage Render
        damageRenderer = this.gameObject.transform.GetChild(3).GetComponent<SpriteRenderer>();
        damageAudio = this.gameObject.transform.GetChild(3).GetComponent<AudioSource>();

        // Bonk Audio
        bonk = this.gameObject.transform.GetChild(4).GetComponent<AudioSource>();

        // Dash Audio
        audioBody = GetComponent<AudioSource>();

        // Camera Shake
        camShake = Camera.main.GetComponent<CameraShake>();
    }

    /*
      Player Movement Script
      This script handles nearly everything to do with the player, from the movement to animations.
      However, This does not handle the picking up of objects. This is handled within th
    */
    private void Update()
    {
        // VISION FUNCTIONS
        // These two functions are used to keep the field of view attached to the player and facing the right way.
        viewField.setOrigin(GetComponent<Renderer>().bounds.center);
        viewField.SetAimDirection(transform.right * -1);

        // GRAB FUNCTIONS
        // These two functions are used to keep the object grabbed in the right direction of the player.
        grabObj.SetAimDirection(transform.up);
        grabObj.SetRotation(transform.eulerAngles);

        // Dash Position
        // Stores the position of where to spawn the dash particle
        particlePosition = this.gameObject.transform.GetChild(6).transform.position;

        // Increase Grace timer [Time between valid damage attacks]
        graceTimer += Time.deltaTime;
        // Increase Charge Timer [Time between jumps]
        chargeDelayTimer += Time.deltaTime;
        // Checks if the dash animation has completed a single loop. And then disables it
        if (anim.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && anim.GetBool("Dashing"))
        {
            anim.SetBool("Dashing", false);
        }

        if (allowMove)
        {
            // ------------MOVEMENT------------
            // START CHARGE JUMP - PRESS W
            // When the player holds W, the player starts to charge the jump.
            if (Input.GetKey(KeyCode.W))
            {
                if (!isCharging && chargeDelayTimer >= chargeDelay)
                {
                    // Reset Charge Timer
                    chargingTimer = 0.0f;
                    // Begin the Charging state
                    isCharging = true;
                    anim.SetBool("Charging", true);
                    hasDashed = false;
                }
            }

            // CHARGING STATE
            // This handles the jump charge amount when the player has entered the charge state.
            // It increases the charge timer based on how long it is charged time.
            if (isCharging)
            {
                // If no audio is playing, play the charging audio.
                if (!(chargeAudio.isPlaying))
                {
                    chargeAudio.clip = a_Charging;
                    chargeAudio.Play();
                }
                // Increase Charger Timer
                chargingTimer += Time.deltaTime;
                // If Charge Timer is equal to the charge length
                if (chargingTimer >= chargeLength)
                {
                    // If the audio clip is charging, then stop it
                    if (chargeAudio.clip == a_Charging)
                    {
                        chargeAudio.Stop();
                    }
                    // Once the audio has stopped, play the fully charged audio.
                    if (!(chargeAudio.isPlaying))
                    {
                        chargeAudio.clip = a_Charged;
                        chargeAudio.Play();
                    }
                    // Change Animation States.
                    anim.SetBool("Charging", false);
                    anim.SetBool("Charged", true);
                    // Keep the Charge Timer to the Charge Length
                    chargingTimer = chargeLength;
                }
            }

            // JUMP FORWARD 
            // When the releases W, they launch forward based on the charge time.
            if (Input.GetKeyUp(KeyCode.W) && !hasDashed)
            {
                hasDashed = true;
                chargeAudio.Stop();

                // Create a particle effect for the dash.
                GameObject gObject = Instantiate(dashParticle, particlePosition, transform.rotation) as GameObject;
                Destroy(gObject, 1);


                anim.SetBool("Charging", false);
                anim.SetBool("Charged", false);
                anim.SetBool("Dashing", true);
                // The Stregth determind by the charge of the jump
                float strength = (chargingTimer / chargeLength);

                // Adds a Force to the player in the direction they are facing
                GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, 1) * strength * jumpStrength * 50);
                //GetComponent<Rigidbody2D>().velocity += new Vector2(0, 1) * strength * jumpStrength;

                // Sets is Charging to false in order to stop increase the charge strength
                isCharging = false;

                chargeDelayTimer = 0.0f;
                audioBody.PlayOneShot(dash, 0.25f);
            }
        }

        

        // ROTATE LEFT
        // Start the turn animation and rotate the player left.
        if (Input.GetKey(KeyCode.A))
        {
            if (!(thrusterAudio.isPlaying))
            {
                thrusterAudio.Play();
            }
            thrusters.SetBool("TurnLeft", true);
            // Rotates the player in the left direction
            transform.Rotate(new Vector3(0, 0, 100f * Time.deltaTime));
        }

        // DISABLE TURN LEFT ANIMATION
        if (Input.GetKeyUp(KeyCode.A))
        {
            thrusters.SetBool("TurnLeft", false);
            thrusterAudio.Stop();
        }

        // ROTATE RIGHT
        // Start the turn right animation and rotate the player right
        if (Input.GetKey(KeyCode.D))
        {
            if (!(thrusterAudio.isPlaying))
            {
                thrusterAudio.Play();
            }
            thrusters.SetBool("TurnRight", true);
            // Rotates the player in the right direction
            transform.Rotate(new Vector3(0, 0, -100f * Time.deltaTime));
        }

        // DISABLE TURN RIGHT ANIMATION
        if (Input.GetKeyUp(KeyCode.D))
        {
            thrusters.SetBool("TurnRight", false);
            thrusterAudio.Stop();
        }
        // ------------END MOVEMENT------------
    }

    // If the player hits an object with the layer of "ENVIRONMENT"
    // Then play the bonk audio.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 8 || collision.gameObject.layer == 1)
          
        {
            bonk.Play();
        }
    }

    // Function to increase corruption value.
    // Able to be called by other objects.
    public void increaseCorruption(float newCorruption)
    {
        // If there has been enough of a delay between attacks
        if (graceTimer >= gracePeriod)
        {
            camShake.Shake(.1f, .15f);

            GameObject damageEffect = Instantiate(damageVisuals) as GameObject;
            Destroy(damageEffect, .25f);
            // play damage audio
            damageAudio.Play();
            // Increase Corruption
            corruption += newCorruption;
            // If corruption is greater than 100, just set it to 100
            if (corruption >= 100)
            {
                AudioSource[] allAudio = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
                foreach (AudioSource listAudio in allAudio)
                {
                    listAudio.volume = 0;
                }
                this.GetComponent<Jumper>().enabled = false;
                Camera.main.transform.GetChild(3).gameObject.transform.GetChild(0).GetComponent<AudioSource>().volume = 1;
                Camera.main.transform.GetChild(3).gameObject.SetActive(true);
                Invoke("RestartGame", 10);
                viewField.GetComponent<MeshRenderer>().enabled = false;
                corruption = 100;
            }
            // Reset Grace Timer
            graceTimer = 0.0f;
            // Update the damage sprite of the player
            updateSprite();
        }
    }

    // Function to decrease corruption value.
    // Able to be called by other objects.
    public void decreaseCorruption(float decreaseValue)
    {
        // Decrease Corruption
        corruption -= decreaseValue;
        // Keep lowest corruption value at 0
        if (corruption < 0)
        {
            corruption = 0;
        }
        // Update the damage sprite of the player
        updateSprite();
    }

    // Updates the player sprite based off of corruption amount.
    // Only called when the player heals or takes damage to increase peformance.
    private void updateSprite()
    {
        // Set the damage state to none if less than 20.
        if (corruption < 20)
        {
            damageRenderer.sprite = null;
        }
        // Set the damage state to one if less than 40.
        else if (corruption < 40)
        {
            damageRenderer.sprite = ADamage;
        }
        // Set the damage state to two if less than 60.
        else if (corruption < 60)
        {
            damageRenderer.sprite = BDamage;
        }
        // Set the damage state to three if less than 80.
        else if (corruption < 80)
        {
            damageRenderer.sprite = CDamage;
        }
        // Set the damage state to four if less than 100.
        else if (corruption < 100)
        {
            damageRenderer.sprite = DDamage;
        }

        if (corruption > 70)
        {
            if (!isCreated)
            {
                highCorruptionEffect = Instantiate(highCorruptionVisuals);
                isCreated = true;
            }
        }
        else
        {
            if (isCreated)
            {
                isCreated = false;
                Destroy(highCorruptionEffect);
            }
        }
    }

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        //EditorSceneManager.LoadScene(EditorSceneManager.GetActiveScene().name);
    }
}
