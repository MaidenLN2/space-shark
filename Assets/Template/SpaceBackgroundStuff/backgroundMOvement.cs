﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backgroundMOvement : MonoBehaviour
{

    public Transform[] backgrounds;
    private float[] scles;
    public float smoothing =1f;
    private Transform cam;
    private Vector3 previouseCamPos;



    // Start is called before the first frame update
    void Awake()
    {
        cam = Camera.main.transform;
    }

    // Update is called once per frame
    void Start()
    {
        previouseCamPos = cam.position;
        scles = new float [backgrounds.Length];
        for (int i = 0; i < backgrounds.Length; i++)
        {

            scles[i] = backgrounds[i].position.z * -1;



        } 
    }
  
    void Update()
    {
        for(int i = 0; i < backgrounds.Length; i++)
        {
            float parallax = (previouseCamPos.x - cam.position.x) * scles[i];
            float parallax2 = (previouseCamPos.y - cam.position.y) * scles[i];

            float backgroundTargetPosX = backgrounds[i].position.x + parallax;
            float backgroundTargetPosY = backgrounds[i].position.y + parallax2;

            Vector3 backgroundTargetPos = new Vector3(backgroundTargetPosX, backgroundTargetPosY, backgrounds[i].position.z);

            backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);


        }


        previouseCamPos = cam.position;




    }
}
